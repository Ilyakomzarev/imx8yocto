#!/bin/sh

cpp -nostdinc -I include -I arch  -undef -x assembler-with-cpp  $1 "$1.dts.prep"

dtc -I dts -O dtb -p 0x1000 "$1.dts.prep" -o "$1.dtb"