#!/bin/bash

get_includes()
{
	cat $1 | grep include | awk '{print $2}'
}

TAB="    "
ARGSCOUNT="$#"
SEARCH="$2"

RED='\033[0;31m'
NC='\033[0m' # No Color

print_includes()
{
	local INC=$(get_includes $1)
	for i in $INC; do
		V=`echo "$i" | sed -e 's/^"//' -e 's/"$//'`
		echo "$2 $i"
		if [ -f "$V" ]; then
		
			if [ $ARGSCOUNT -eq 2 ]; then
				local FOUND=`cat $V | grep $SEARCH`
				if [ -n "$FOUND" ]; then
					echo -e "$2 $TAB $TAB $TAB ${RED}$FOUND${NC}"
				fi
			fi
			
			print_includes $V "$2 $TAB"
		fi
		
		V=`echo "$i" | sed -e 's/^<//' -e 's/>$//'`
		if [ -f "$V" ]; then
			print_includes $V "$2 $TAB"
		fi

	done
}

echo $1
print_includes $1 $TAB

