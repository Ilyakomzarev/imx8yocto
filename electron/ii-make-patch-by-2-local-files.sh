#!/bin/sh

git diff --no-index --patch $1 $2 > "0000-$1.patch"