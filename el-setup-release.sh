#!/bin/sh

EL_ARG_DIR=tqma8xxs_build

source fsl-setup-release.sh -b $EL_ARG_DIR -e wayland

cd ../electron

TMP=`pwd`
PATH=$PATH:$TMP

cp bblayers.conf ../$EL_ARG_DIR/conf/bblayers.conf
cp local.conf ../$EL_ARG_DIR/conf/local.conf

cd ../$EL_ARG_DIR

echo "bitbake el-image-detector"
echo "bitbake el-image-minimal"