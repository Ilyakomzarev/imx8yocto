SRC_URI += "file://ath10k.cfg;subdir=git \
            file://pcidebug.cfg;subdir=git \
            file://tqma8qx-regulator-support.cfg;subdir=git \
            file://gpio-enablement.cfg;subdir=git \
            file://general.cfg;subdir=git \
            file://bridge.cfg;subdir=git \
            file://0000-remove-no-ir-flag.patch;patch=1 \
            file://fsl-imx8qxp-tqma8xqps-mb-smarc-2-pci.dts;subdir=git/arch/${ARCH}/boot/dts \
			file://0001-tqma8xxs-common.dtsi.patch;patch=1 \
            "
			
FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

KERNEL_DEVICETREE += "fsl-imx8qxp-tqma8xqps-mb-smarc-2-pci.dtb"

PACKAGE_ARCH = "${MACHINE_ARCH}"

