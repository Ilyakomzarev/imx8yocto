SRC_URI += "file://ath10k.cfg;subdir=git \
            file://atom.cfg;subdir=git \
            file://remove_useless.cfg;subdir=git \
            file://pci.cfg;subdir=git \
            file://general.cfg;subdir=git \
			file://bridge.cfg;subdir=git \
            file://0000-remove-no-ir-flag.patch;patch=1 \
            "

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"


