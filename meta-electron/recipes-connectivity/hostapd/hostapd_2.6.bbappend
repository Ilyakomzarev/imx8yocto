SRC_URI += "file://hostapd.conf;subdir=hostapd-2.6 \
			file://defconfig"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

do_install_append(){
	install -m 644 ${WORKDIR}/hostapd-2.6/hostapd.conf ${D}${sysconfdir}/
}