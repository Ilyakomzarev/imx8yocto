require recipes-core/images/core-image-minimal.bb

SUMMARY =  "This is Elektron Linux Image"

IMAGE_LINGUAS_append = " en-us "

LICENSE = "MIT"

inherit distro_features_check

IMAGE_INSTALL += "\
    packagegroup-base \
    packagegroup-hwutils \
    packagegroup-fsutils \
    packagegroup-sysutils \
    ${@bb.utils.contains('MACHINE_FEATURES', 'can', ' packagegroup-can', '', d)} \
"

IMAGE_INSTALL += " iw libpciaccess libpciaccess pciutils libtool iptables setserial iperf3 hostapd iproute2 dnsmasq ethtool strace gdbserver \
				linux-firmware-ath10k kernel-module-ath10k-pci kernel-module-ath10k-core kernel-module-ath "
				
EXTRA_IMAGE_FEATURES += " package-management ssh-server-dropbear"
#KERNEL_MODULE_AUTOLOAD += "ath10k_pci" #not working

ROOTFS_POSTPROCESS_COMMAND += "postprocessrootfs;"
postprocessrootfs() {
	 echo "ath10k_pci" > ${IMAGE_ROOTFS}/etc/modules
	 
     echo "net.core.wmem_max=4194304" > ${IMAGE_ROOTFS}/etc/sysctl.conf
	 echo "net.core.rmem_max=12582912" >> ${IMAGE_ROOTFS}/etc/sysctl.conf
	 echo "net.ipv4.tcp_rmem = 65536 262144 4194304" >> ${IMAGE_ROOTFS}/etc/sysctl.conf
	 echo "net.ipv4.tcp_wmem = 65536 262144 4194304" >> ${IMAGE_ROOTFS}/etc/sysctl.conf
	 echo "net.ipv4.tcp_slow_start_after_idle = 0" >> ${IMAGE_ROOTFS}/etc/sysctl.conf
	 echo "net.ipv4.ip_forward = 0" >> ${IMAGE_ROOTFS}/etc/sysctl.conf
}

